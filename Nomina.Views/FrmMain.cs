﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmMain : Form
    {
        private DaoEmpleadoImplements daoEmpleadoImplements;
        private List<Empleado> empleados;
        private BindingSource bsEmpleados;
        public FrmMain()
        {
            daoEmpleadoImplements = new DaoEmpleadoImplements();
            bsEmpleados = new BindingSource();
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            empleados = daoEmpleadoImplements.All();

            empleados.ForEach(ep => {
                dsNomina.Tables["Empleado"].Rows.Add(ep.EmpleadoAsArray());
            });

            bsEmpleados.DataSource = dsNomina;
            bsEmpleados.DataMember = dsNomina.Tables["Empleado"].TableName;

            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;
        }

        //Nuevo
        private void button1_Click(object sender, EventArgs e)
        {
            FrmEmpleado frmEmpleado = new FrmEmpleado();
            frmEmpleado.DSNomina = dsNomina;
            frmEmpleado.ShowDialog(this);
        }
        //Eliminar
        private void button3_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvEmpleados.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int index = dgvEmpleados.SelectedRows[0].Index;
            int id = Convert.ToInt32(dgvEmpleados.Rows[index].Cells["Id"].Value.ToString());
            string cedula = dgvEmpleados.Rows[index].Cells["Cedula"].Value.ToString();
            string nombres = dgvEmpleados.Rows[index].Cells["Nombres"].Value.ToString();
            string apellidos = dgvEmpleados.Rows[index].Cells["Apellidos"].Value.ToString();
            string direccion = dgvEmpleados.Rows[index].Cells["Direccion"].Value.ToString();
            string telefono = dgvEmpleados.Rows[index].Cells["Telefono"].Value.ToString();
            DateTime fechadecontratacion = Convert.ToDateTime(dgvEmpleados.Rows[index].Cells["FechaContratacion"].Value.ToString());
            decimal salario = Convert.ToDecimal(dgvEmpleados.Rows[index].Cells["Salario"].Value.ToString());
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            Empleado empleado = new Empleado()
            {
                Id = id,
                Nombres = nombres,
                Apellidos = apellidos,
                Cedula = cedula,
                Telefono = telefono,
                Direccion = direccion,
                Salario = salario,
                FechaContratacion = fechadecontratacion
            };

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                dgvEmpleados.Rows.RemoveAt(index);
                daoEmpleadoImplements.Delete(empleado);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        //editar
        private void button2_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvEmpleados.SelectedRows;
            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int index = dgvEmpleados.SelectedRows[0].Index;
            int id = Convert.ToInt32(dgvEmpleados.Rows[index].Cells["Id"].Value.ToString());
            string cedula = dgvEmpleados.Rows[index].Cells["Cedula"].Value.ToString();
            string nombres = dgvEmpleados.Rows[index].Cells["Nombres"].Value.ToString();
            string apellidos = dgvEmpleados.Rows[index].Cells["Apellidos"].Value.ToString();
            string direccion = dgvEmpleados.Rows[index].Cells["Direccion"].Value.ToString();
            string telefono = dgvEmpleados.Rows[index].Cells["Telefono"].Value.ToString();
            DateTime fechadecontratacion = Convert.ToDateTime(dgvEmpleados.Rows[index].Cells["FechaContratacion"].Value.ToString());
            decimal salario = Convert.ToDecimal(dgvEmpleados.Rows[index].Cells["Salario"].Value.ToString());
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            Empleado empleado = new Empleado()
            {
                Id = id,
                Nombres = nombres,
                Apellidos = apellidos,
                Cedula = cedula,
                Telefono = telefono,
                Direccion = direccion,
                Salario = salario,
                FechaContratacion = fechadecontratacion
            };
            FrmEmpleado frm = new FrmEmpleado();
            frm.SetEmpleado(empleado, index);
            frm.DSNomina = dsNomina;
            frm.ShowDialog();
        }
    }
}
