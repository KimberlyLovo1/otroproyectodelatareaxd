﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using Nomina.Views.Extensiones;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmEmpleado : Form
    {
        public DataSet DSNomina { get; set; }
        private DaoEmpleadoImplements daoEmpleadoImplements;
        private DataRow drEmpleado;
        public int index;
        public int indexActual;

        public FrmEmpleado()
        {
            daoEmpleadoImplements = new DaoEmpleadoImplements();
            InitializeComponent();
        }
        
        public void SetEmpleado(Empleado e, int index)
        {
            txtCedula.Text = e.Cedula;
            txtName.Text = e.Nombres;
            txtApellido.Text = e.Apellidos;
            txtdirect.Text = e.Direccion;
            txtNum.Text = e.Telefono;
            txtMoney.Text = e.Salario.ToString();
            tiempo.Value = e.FechaContratacion;
            this.index = index;
            this.indexActual = e.Id;
        }

        //Añadir
        private void button2_Click(object sender, EventArgs e)
        {
            drEmpleado = DSNomina.Tables["Empleado"].NewRow();
            if (txtName.Text == "" || txtApellido.Text == "" || txtCedula.Text == "" || txtNum.Text == "" || txtdirect.Text == "" || txtMoney.Text == "")
            {
                MessageBox.Show("Llénelo xd", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            //Aun no se porque no da xd
            Empleado empleado = new Empleado()
            {
                Id = daoEmpleadoImplements.Guardarid() + 1,
                Nombres = txtName.Text,
                Apellidos = txtApellido.Text,
                Cedula = txtCedula.Text,
                Telefono = txtNum.Text,
                Direccion = txtdirect.Text,
                Salario = decimal.Parse(txtMoney.Text),
                FechaContratacion = tiempo.Value
            };

            drEmpleado["Id"] = empleado.Id;
            drEmpleado["Cedula"] = empleado.Cedula;
            drEmpleado["Nombres"] = empleado.Nombres;
            drEmpleado["Apellidos"] = empleado.Apellidos;
            drEmpleado["Direccion"] = empleado.Direccion;
            drEmpleado["Telefono"] = empleado.Telefono;
            drEmpleado["FechaContratacion"] = empleado.FechaContratacion;
            drEmpleado["Salario"] = empleado.Salario;

            if (daoEmpleadoImplements.findByCedula(empleado.Cedula) != null)
            {
                MessageBox.Show("Error, Numero de cedula duplicada", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            daoEmpleadoImplements.Create(empleado);
            DSNomina.Tables["Empleado"].Rows.Add(drEmpleado);
            Dispose();
        } 

        //editar
        private void button1_Click(object sender, EventArgs e)
        {
            drEmpleado = DSNomina.Tables["Empleado"].NewRow();
            {
                MessageBox.Show("Llénelo xd", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Empleado empleado = new Empleado()
            {
                Id = indexActual,
                Nombres = txtName.Text,
                Apellidos = txtApellido.Text,
                Cedula = txtCedula.Text,
                Telefono = txtNum.Text,
                Direccion = txtdirect.Text,
                Salario = Convert.ToDecimal(txtMoney.Text),
                FechaContratacion = tiempo.Value
            };
            drEmpleado["Id"] = indexActual;
            drEmpleado["Cedula"] = empleado.Cedula;
            drEmpleado["Nombres"] = empleado.Nombres;
            drEmpleado["Apellidos"] = empleado.Apellidos;
            drEmpleado["Direccion"] = empleado.Direccion;
            drEmpleado["Telefono"] = empleado.Telefono;
            drEmpleado["FechaContratacion"] = empleado.FechaContratacion;
            drEmpleado["Salario"] = empleado.Salario;
            
            daoEmpleadoImplements.Update(empleado);
            
            DSNomina.Tables["Empleado"].Rows.RemoveAt(index);
            DSNomina.Tables["Empleado"].Rows.InsertAt(drEmpleado, index);
            Dispose();
        }
    }
}
